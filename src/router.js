import Vue from 'vue';
import Router from 'vue-router';
import ClientWorkListing from './views/ClientWorkListing.vue';
import ExperimentsListing from './views/ExperimentsListing.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: {
                name: 'client-work',
            },
        },
        {
            name: 'client-work',
            path: '/client-work',
            component: ClientWorkListing,
        },
        {
            name: 'experiments',
            path: '/experiments',
            component: ExperimentsListing,
        },
        {
            name: 'about',
            path: '/about',
            component: About,
        },
    ],
});
