// Breakpoints mixin
// -----------------
// Fetch all of the CSS custom properties that have the
// media query breakpoint values in them, and load them in to an object.

export default {
    computed: {
        breakpoints() {
            const styles = window.getComputedStyle(document.documentElement);

            return {
                ti: styles.getPropertyValue('--breakpoint-ti').replace('px', ''),
                xs: styles.getPropertyValue('--breakpoint-xs').replace('px', ''),
                sm: styles.getPropertyValue('--breakpoint-sm').replace('px', ''),
                md: styles.getPropertyValue('--breakpoint-md').replace('px', ''),
                ml: styles.getPropertyValue('--breakpoint-ml').replace('px', ''),
                lg: styles.getPropertyValue('--breakpoint-lg').replace('px', ''),
                xl: styles.getPropertyValue('--breakpoint-xl').replace('px', ''),
            };
        },
    },
};
