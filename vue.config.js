// vue.config.js

const path = require('path');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const PrerenderSPAPlugin = require('prerender-spa-plugin');
const Terser = require('terser');

const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;

module.exports = {
    assetsDir: 'assets',
    css: {
        loaderOptions: {
            // pass options to sass-loader
            sass: {
                // @/ is an alias to src/
                // Include variables file
                data: `
                    @import "@/variables.scss";
                    @import "@/mixins.scss";
                    $env: ${process.env.NODE_ENV};
                `,
            },
        },
    },
    configureWebpack: {
        plugins: [
            // Config svg sprite loader plugin
            new SpriteLoaderPlugin(),

            // Config Imagemin plugin
            new ImageminPlugin({
                disable: process.env.NODE_ENV !== 'production', // Disable during development
            }),

            // Config Prerender SPA plugin
            new PrerenderSPAPlugin({
                staticDir: path.join(__dirname, 'dist'),
                routes: ['/', '/client-work', '/experiments', '/about'],
                renderer: new Renderer({
                    injectProperty: '__PRERENDER_INJECTED',
                    inject: {
                        prerendered: true,
                    },
                }),
                minify: {
                    collapseWhitespace: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true,
                    minifyJS: text => Terser.minify(text).code,
                },
            }),
        ],
    },
    chainWebpack: (config) => {
        // Remove the default SVG file rules
        config.module.rules.delete('svg');

        // Config svg-sprite-loader to build a spritesheet from processed svgs
        config.module
            .rule('svg')
            .test(/\.svg$/)
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({
                extract: true,
                spriteFilename: 'assets/svg/icons.svg',
            });

        // Remove the default image rules
        config.module.rules.delete('images');

        // Config responsive-loader to process images
        config.module
            .rule('images')
            .test(/\.(jpe?g|png)$/i)
            .use('responsive-loader')
            .loader('responsive-loader')
            .options({
                outputPath: 'assets/img/',
                name: '[name].[width].[hash:8].[ext]',
                format: 'jpg',
                // Quality set to 95 visually looks perfect, but 85 is much more
                // performant and is worth the slight artifacts.
                // TODO: Revise this once webp is more widly supported by
                // browsers and the 'responsive-loader' package.
                quality: '85',
                sizes: [300, 633],
                placeholder: true,
                placeholderSize: 50,
            });
    },
};
