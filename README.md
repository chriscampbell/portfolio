# Portfolio

This project is my personal portfolio, it's a small Vue CLI app with a 
deliberately simple and efficient design focused on performance and ease of use.

The page content is loaded from a .json file which Vue then parses to generate 
the layout, menu structure and content of the site.

Images are loaded responsively using the srcset attribute and include an inline 
SVG which acts as a placeholder until the correct image asset is loaded which is
then faded in. The various image sizes required to cover all devices are 
generated using a webpack task from an original source image.

During the webpack production build all pages are parsed through a prerender 
plugin to export them as static minified html which helps reduce initial load 
times.

Gitlab build pipelines are used for continuous deployment of the site.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```
or launch the project using Vue CLI web ui with
```
vue ui
```

## Production builds
Uses the gitlab continuous deployment pipeline, see `.gitlab-ci.yml` for 
configuration options.